node {

  properties ([
    disableConcurrentBuilds()
  ])

  stage ('Approval for Deploy Production') {
    timeout(time: 5, unit: 'MINUTES') {
      input(id: "Deploy Production", message: "Deploy Production ?", ok: 'Deploy')
    }
  }

  stage ('Git Checkout') {
    try {
      deleteDir()

      git branch: 'master',
      credentialsId: '7bff3d8d-87f0-4276-be97-b936fd3f7e7f',
      url: 'https://bitbucket.org/sarayutink/ansible.git'
            
      currentBuild.result = 'SUCCESS'
    }
    catch (Exception err) {
      currentBuild.result = "FAILURE"
    }
  }

  stage ('Deploy to Prod Server') {
    try {
      withCredentials([string(credentialsId: 'SSH-DEPLOY', variable: 'PASS')]) {
        ansiblePlaybook(
          playbook: 'deploy.yml',
          inventory: 'host.ini',
          disableHostKeyChecking: true,
          extras: '-l prod-sp3-api-tt2',
          extraVars: [
            ansible_ssh_pass: "${PASS}"
          ]
        )
      }
            
      currentBuild.result = 'SUCCESS'
    }
    catch (Exception err) {
      currentBuild.result = "FAILURE"
    }
  }
  
  stage ('Clear Workspace Demo') {
    try {
      cleanWs()

      currentBuild.result = 'SUCCESS'
    }
    catch (Exception err) {
      currentBuild.result = "FAILURE"
    }
  }
}

 