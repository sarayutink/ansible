node {

  properties ([
    disableConcurrentBuilds()
  ])

  stage ('Approval for Deploy Production') {
    timeout(time: 5, unit: 'MINUTES') {
      input(id: "Deploy Production", message: "Deploy Production ?", ok: 'Deploy')
    }
  }

  stage ('Git Checkout') {
    try {
      deleteDir()

      git branch: 'master',
      credentialsId: 'deee2a82-0a5f-4cd3-bd0a-bb6623876a5e',
      url: 'https://bitbucket.org/sarayutink/ansible.git'
            
      currentBuild.result = 'SUCCESS'
    }
    catch (Exception err) {
      currentBuild.result = "FAILURE"
    }
  }

  stage ('Deploy to Prod Server') {
    try {
      withCredentials([string(credentialsId: 'SSH-DEPLOY', variable: 'PASS')]) {
        ansiblePlaybook(
          playbook: 'deploy.yml',
          inventory: 'host.ini',
          disableHostKeyChecking: true,
          extras: '-l prod-common-api-mtg',
          extraVars: [
            ansible_ssh_pass: "${PASS}"
          ]
        )
      }
            
      currentBuild.result = 'SUCCESS'
    }
    catch (Exception err) {
      currentBuild.result = "FAILURE"
    }
  }
  
  stage ('Clear Workspace Demo') {
    try {
      cleanWs()

      currentBuild.result = 'SUCCESS'
    }
    catch (Exception err) {
      currentBuild.result = "FAILURE"
    }
  }
}

 