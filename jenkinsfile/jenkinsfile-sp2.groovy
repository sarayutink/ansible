node {

  properties ([
    disableConcurrentBuilds()
  ])

  if (env.BRANCH_NAME == 'alpha') {
    stage ('Git Checkout') {
      try {
        deleteDir()

        git branch: 'master',
        credentialsId: '8f9ae818-dc6b-47dc-a5ad-9de76f1e72c6',
        url: 'https://bitbucket.org/sarayutink/ansible.git'
            
        currentBuild.result = 'SUCCESS'
      }
      catch (Exception err) {
        currentBuild.result = "FAILURE"
      }
    }

    stage ('Deploy to Alpha Server') {
      try {
        withCredentials([string(credentialsId: 'SSH-DEPLOY', variable: 'PASS')]) {
          ansiblePlaybook(
            playbook: 'deploy.yml',
            inventory: 'host.ini',
            disableHostKeyChecking: true,
            extras: '-l alpha-sp2-api',
            extraVars: [
              ansible_ssh_pass: "${PASS}"
            ]
          )
        }
            
        currentBuild.result = 'SUCCESS'
      }
      catch (Exception err) {
        currentBuild.result = "FAILURE"
      }
    }
  
    stage ('Clear Workspace Demo') {
      try {
        cleanWs()

        currentBuild.result = 'SUCCESS'
      }
      catch (Exception err) {
        currentBuild.result = "FAILURE"
      }
    }
  }

  if (env.BRANCH_NAME == 'pre-prod') {
    stage ('Git Checkout') {
      try {
        deleteDir()

        git branch: 'master',
        credentialsId: '8f9ae818-dc6b-47dc-a5ad-9de76f1e72c6',
        url: 'https://bitbucket.org/sarayutink/ansible.git'
            
        currentBuild.result = 'SUCCESS'
      }
      catch (Exception err) {
        currentBuild.result = "FAILURE"
      }
    }

    stage ('Deploy to Preprod Server') {
      try {
        withCredentials([string(credentialsId: 'SSH-DEPLOY', variable: 'PASS')]) {
          ansiblePlaybook(
            playbook: 'deploy.yml',
            inventory: 'host.ini',
            disableHostKeyChecking: true,
            extras: '-l preprod-sp2-api',
            extraVars: [
              ansible_ssh_pass: "${PASS}"
            ]
          )
        }
            
        currentBuild.result = 'SUCCESS'
      }
      catch (Exception err) {
        currentBuild.result = "FAILURE"
      }
    }
  
    stage ('Clear Workspace Demo') {
      try {
        cleanWs()

        currentBuild.result = 'SUCCESS'
      }
      catch (Exception err) {
        currentBuild.result = "FAILURE"
      }
    }
  }

  if (env.BRANCH_NAME == 'master') {
    stage ('Approval for Deploy Production') {
      timeout(time: 5, unit: 'MINUTES') {
        notify("Waiting to Approve")
        input(id: "Deploy Production", message: "Deploy Production ?", ok: 'Deploy')
      }
    }

    stage ('Git Checkout') {
      try {
        deleteDir()

        git branch: 'master',
        credentialsId: '8f9ae818-dc6b-47dc-a5ad-9de76f1e72c6',
        url: 'https://bitbucket.org/sarayutink/ansible.git'
            
        currentBuild.result = 'SUCCESS'
      }
      catch (Exception err) {
        currentBuild.result = "FAILURE"
      }
    }

    stage ('Deploy to Prod Server') {
      try {
        withCredentials([string(credentialsId: 'SSH-DEPLOY', variable: 'PASS')]) {
          ansiblePlaybook(
            playbook: 'deploy.yml',
            inventory: 'host.ini',
            disableHostKeyChecking: true,
            extras: '-l prod-sp2-api',
            extraVars: [
              ansible_ssh_pass: "${PASS}"
            ]
          )
        }
            
        currentBuild.result = 'SUCCESS'
      }
      catch (Exception err) {
        currentBuild.result = "FAILURE"
      }
    }
  
    stage ('Clear Workspace Demo') {
      try {
        cleanWs()

        currentBuild.result = 'SUCCESS'
      }
      catch (Exception err) {
        currentBuild.result = "FAILURE"
      }
    }
  }
}

 